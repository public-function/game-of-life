import styles from './CellField.module.css';

type CellFieldProps = {
    size?: number
    alive: boolean
    onClick: () => void
}

export default function CellField({size = 10, alive, onClick}: CellFieldProps) {
    return <div
        style={
            {
                width: size,
                height: size,
            }
        }
        onClick={onClick}
        className={`${styles.cell} ${alive ? styles.life : styles.dead}`}
    />
}