import Habitat from "./Habitat";
import {useTimer} from "@/hooks/use-timer";
import {useHabitatState} from "@/hooks/use-habitat-state";

import styles from './Game.module.css';
import {buildHabitat} from "@/utils/habitat";
import Button from "@/components/Button";
import {useEffect, useState} from "react";
import SpeedSelection from "./SpeedSelection";
import {patternLibrary} from "@/model/patterms";
import {detectPattern} from "@/utils/pattern";

type GameProps = {
    patternIds: string[]
    onPatternIdsChange: (ids: string[]) => void
}

export default function Game(
    {
        patternIds,
        onPatternIdsChange
    }: GameProps
){

    const {
        state: habitatState,
        toggleCell,
        evolve,
    } = useHabitatState(
        buildHabitat(
            40,40,
            [{x:1,y:2},{x:1,y:3},{x:1,y:4}]
        )
    );

    const [speed, setSpeed] = useState(300);

    const {
        isRunning,
        resume,
        pause,
    } = useTimer(()=>{
        evolve()
    }, speed);

    useEffect(()=> {
        const candidates = patternLibrary.filter(pattern => !patternIds.includes(pattern.id));
        const found = candidates.filter(pattern => {
            return detectPattern(habitatState, pattern);
        });
        if(found.length){
            const set = new Set([
                ...patternIds,
                ...found.map(p=>p.id),
            ]);
            onPatternIdsChange([...Array.from(set)]);
        }
    }, [JSON.stringify(habitatState)]);

    return <div className={styles.root}>

        <div className={styles.controls}>
            <label>
                Evolution speed<br/>
                <SpeedSelection value={speed} onChange={setSpeed} />
            </label>
            {isRunning ?
                <Button onClick={pause}>Pause</Button>
                :
                <Button onClick={resume}>Start</Button>
            }
        </div>

        <Habitat
            data={habitatState}
            onCellClick={(x,y)=>{
                toggleCell(x,y);
            }}
        />

        <div>
            Explored patterns: {patternIds.length}
            <ul className={styles.idsList}>
                {patternIds.map(id=>{
                    return <li key={id}>{id}</li>
                })}
            </ul>
        </div>

    </div>
}
