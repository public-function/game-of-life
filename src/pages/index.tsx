import Head from 'next/head'
import Game from "../components/Game";

import styles from './Index.module.css'
import {useMyPatternIds} from "@/hooks/use-patterns";


export default function Home() {

    const {
        patternIds,
        setPatternIds,
    } = useMyPatternIds();

    return (
        <>
            <Head>
                <title>Game of Life</title>
                {/*<link rel="icon" href="/favicon.ico"/>*/}
            </Head>

            <div className={styles.root}>
                <Game
                    patternIds={patternIds}
                    onPatternIdsChange={setPatternIds}
                />
            </div>
        </>
    )
}
