import {Pattern} from "@/types";
import {getRotations} from "@/utils/matrix";

export const gliderPattern: Pattern = {
    id: "glider",
    variants: [
        ...getRotations(
            [
                [0,0,1],
                [1,1,0],
                [0,1,1],
            ],
            true
        ),
        ...getRotations(
            [
                [1,0,1],
                [1,1,0],
                [0,1,0],
            ],
            true
        ),
        ...getRotations(
            [
                [0,1,0],
                [1,0,0],
                [1,1,1],
            ],
            true
        ),
        ...getRotations(
            [
                [1,0,0],
                [1,0,1],
                [1,1,0],
            ],
            true
        )
    ]
};


export const middleWeightSpaceShipPattern: Pattern = {
    id: "middle-weight-space-ship",
    variants: [
        ...getRotations(
            [
                [0,0,1,1,1,0],
                [0,1,1,1,1,1],
                [1,1,0,1,1,1],
                [0,1,1,0,0,0],
            ],
            true
        ),
    ]
};
