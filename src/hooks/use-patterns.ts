import {useEffect, useState} from "react";

const getCache = (): string[] => {
    const saved = localStorage.getItem("patternIds");
    return saved ? JSON.parse(saved) : [];
}

const setCache = (ids: string[]) => {
    const cache = getCache();

    localStorage.setItem("patternIds", JSON.stringify(
        Array.from(new Set([
            ...cache,
            ...ids
        ]))
    ));
}

export const useMyPatternIds = () => {
    const [state, setState] = useState<string[]>([]);

    useEffect(function () {
        setState(getCache());
    }, []);

    return {
        patternIds: state,
        setPatternIds: (value: string[]) => {
            setCache(value);
            setState(value);
        },
    }
}